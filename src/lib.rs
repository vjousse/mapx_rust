#![feature(plugin)]
#![plugin(rocket_codegen)]

#[macro_use]
extern crate diesel;
extern crate diesel_geometry;
extern crate rocket;

pub mod db;
pub mod models;
pub mod schema;

use diesel::RunQueryDsl;
use models::Entry;

#[get("/")]
fn index(connection: db::DbConn) -> &'static str {
    use schema::entry::dsl::*;

    let results = entry
        .load::<Entry>(&*connection)
        .expect("Error loading entries");

    println!("Displaying {} entries", results.len());
    for result in results {
        println!("{}", result.name);
        println!("----------\n");
        println!("{:?}", result.location);
    }
    "Hello, world!"
}
